package ru.evstigneev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.IProjectService;
import ru.evstigneev.tm.comparator.ComparatorProjectDateCreation;
import ru.evstigneev.tm.comparator.ComparatorProjectDateFinish;
import ru.evstigneev.tm.comparator.ComparatorProjectDateStart;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.DateParser;
import ru.evstigneev.tm.util.MyBatisUtil;

import java.util.*;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @NotNull
    private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @NotNull
    private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public Collection<Project> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll();
        }
    }

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectName,
                          @NotNull final String description) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(description);
        project.setDateOfCreation(new Date());
        project.setStatus(Status.PLANNING);
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.persist(project)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public boolean persist(@NotNull Project project) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.persist(project)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.deleteAllProjectTasks(userId, projectId);
            if (projectRepository.remove(userId, projectId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String projectId,
                          @NotNull final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish,
                          @NotNull final Status status) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.update(userId, projectId, name, description, DateParser.setDateByString(dateStart),
                    DateParser.setDateByString(dateFinish), status)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findOne(userId, projectId);
        }
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAllByUserId(userId);
        }
    }

    @Override
    public void removeAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAll();
            projectRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAllByUserId(userId);
            if (projectRepository.removeAllByUserId(userId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean merge(Project project) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (projectRepository.findOne(project.getUserId(), project.getId()) == null) {
                projectRepository.persist(project);
                sqlSession.commit();
                return true;
            } else {
                projectRepository.update(project.getId(), project.getUserId(), project.getName(),
                        project.getDescription(), project.getDateStart(), project.getDateFinish(), project.getStatus());
                sqlSession.commit();
                return true;
            }
        }
    }

    public List<Project> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<Project> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<Project> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Project> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorProjectDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorProjectDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorProjectDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<Project> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (Project project : findAll()) {
            if (project.getName().contains(string) || (project.getDescription() != null && project.getDescription().contains(string))) {
                projectList.add(project);
            }
        }
        return projectList;
    }

}
