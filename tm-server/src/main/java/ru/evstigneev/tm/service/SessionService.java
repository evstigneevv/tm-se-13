package ru.evstigneev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.ISessionService;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.exception.LoginException;
import ru.evstigneev.tm.exception.NoPermissionException;
import ru.evstigneev.tm.util.MyBatisUtil;
import ru.evstigneev.tm.util.SignatureCalculator;

import java.util.UUID;

public class SessionService implements ISessionService {

    @NotNull
    private static final long SESSION_EXPIRATION_TIME = 1_800_000L;

    @NotNull
    private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @NotNull
    private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public void validate(@Nullable final Session session) throws Exception {
        if (session == null) throw new LoginException();
        if (session.getSignature() == null || session.getId().isEmpty() || session.getUserId().isEmpty()
                || session.getRoleType().displayName().isEmpty() || session.getSignature().isEmpty()) {
            throw new LoginException();
        }
        if (System.currentTimeMillis() - session.getTimestamp() > SESSION_EXPIRATION_TIME) {
            throw new LoginException();
        }
        @NotNull final Session temp = session.clone();
        temp.setSignature(null);
        temp.setSignature(SignatureCalculator.calculateSignature(temp));
        if (temp.getSignature() != null && !temp.getSignature().equals(session.getSignature())) {
            throw new NoPermissionException();
        }
    }

    @Override
    public void validate(@Nullable final Session session, @NotNull final RoleType roleType) throws Exception {
        validate(session);
        if (!session.getRoleType().equals(roleType)) throw new NoPermissionException();
    }

    @Override
    public Session openSession(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @NotNull final Session session = new Session();
            @NotNull final User user = userRepository.findByLogin(login);
            session.setUserId(user.getId());
            session.setId(UUID.randomUUID().toString());
            session.setRoleType(user.getRole());
            session.setTimestamp(System.currentTimeMillis());
            session.setSignature(SignatureCalculator.calculateSignature(session));
            sessionRepository.persist(session);
            sqlSession.commit();
            return session;
        }
    }

    @Override
    public boolean closeSession(@NotNull final Session session) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeByUserId(session.getUserId());
            return true;
        }
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final Session session) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (sessionRepository.persist(session)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean removeByUserId(@NotNull final String userId) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (sessionRepository.removeByUserId(userId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

}