package ru.evstigneev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.service.ITaskService;
import ru.evstigneev.tm.comparator.ComparatorTaskDateCreation;
import ru.evstigneev.tm.comparator.ComparatorTaskDateFinish;
import ru.evstigneev.tm.comparator.ComparatorTaskDateStart;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;
import ru.evstigneev.tm.exception.CommandCorruptException;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.DateParser;
import ru.evstigneev.tm.util.MyBatisUtil;

import java.util.*;

public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private static final String DATE_START_COMPARATOR_NAME = "date start";
    @NotNull
    private static final String DATE_FINISH_COMPARATOR_NAME = "date finish";
    @NotNull
    private static final String DATE_CREATION_COMPARATOR_NAME = "date creation";
    @NotNull
    private static final String STATUS_COMPARATOR_NAME = "status";

    @NotNull
    private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @NotNull
    private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public boolean create(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
                          @NotNull final String description) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(UUID.randomUUID().toString());
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName(name);
        task.setDescription(description);
        task.setDateOfCreation(new Date());
        task.setStatus(Status.PLANNING);
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.persist(task)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public Collection<Task> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAll();
        }
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findAllByUserId(userId);
        }
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (userId.isEmpty() || taskId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.remove(userId, taskId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public Collection<Task> getTaskListByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.getTaskListByProjectId(userId, projectId);
        }
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String taskId,
                          @NotNull final String name, @Nullable final String description,
                          @Nullable final String dateStart, @Nullable final String dateFinish,
                          @NotNull final Status status) throws Exception {
        if (userId.isEmpty() || taskId.isEmpty() || name.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.update(userId, taskId, name, description, DateParser.setDateByString(dateStart),
                    DateParser.setDateByString(dateFinish), status)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean merge(@NotNull final String userId, @NotNull final Task task) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.findOne(task.getUserId(), task.getProjectId(), task.getId()) == null) {
                taskRepository.persist(task);
                sqlSession.commit();
                return true;
            } else {
                taskRepository.update(task.getUserId(), task.getId(), task.getName(),
                        task.getDescription(), task.getDateStart(), task.getDateFinish(), task.getStatus());
                sqlSession.commit();
                return true;
            }
        }
    }

    @Override
    public boolean persist(@NotNull final String userId, @NotNull final Task task) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.persist(task)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public void removeAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            taskRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public boolean removeAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.removeAllByUserId(userId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean deleteAllProjectTasks(@NotNull final String userId, @NotNull final String projectId) throws
            Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            if (taskRepository.deleteAllProjectTasks(userId, projectId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String projectId,
                        @NotNull final String taskId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || taskId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);
            return taskRepository.findOne(userId, projectId, taskId);
        }
    }

    public List<Task> sort(@NotNull final String comparatorName) throws Exception {
        @NotNull final List<Task> sortedList = new ArrayList<>(findAll());
        @Nullable final Comparator<Task> comparator = findComparator(comparatorName);
        if (comparator == null) {
            Collections.sort(sortedList);
            return sortedList;
        }
        Collections.sort(sortedList, comparator);
        return sortedList;
    }

    private Comparator<Task> findComparator(@NotNull final String name) throws CommandCorruptException {
        switch (name.toLowerCase()) {
            default:
                throw new CommandCorruptException();
            case DATE_START_COMPARATOR_NAME: {
                return new ComparatorTaskDateStart();
            }
            case DATE_FINISH_COMPARATOR_NAME: {
                return new ComparatorTaskDateFinish();
            }
            case DATE_CREATION_COMPARATOR_NAME: {
                return new ComparatorTaskDateCreation();
            }
            case STATUS_COMPARATOR_NAME: {
                return null;
            }
        }
    }

    public List<Task> searchByString(@NotNull final String string) throws Exception {
        if (string.isEmpty()) throw new EmptyStringException();
        @Nullable final List<Task> projectList = new ArrayList<>();
        for (Task task : findAll()) {
            if (task.getName().contains(string) || (task.getDescription() != null && task.getDescription().contains(string))) {
                projectList.add(task);
            }
        }
        return projectList;
    }

}
