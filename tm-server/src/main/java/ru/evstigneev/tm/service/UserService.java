package ru.evstigneev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.api.service.IUserService;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;
import ru.evstigneev.tm.util.MyBatisUtil;
import ru.evstigneev.tm.util.PasswordParser;

import java.util.Collection;
import java.util.UUID;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final MyBatisUtil myBatisUtil = new MyBatisUtil();
    @NotNull
    private final SqlSessionFactory sessionFactory = myBatisUtil.getSqlSessionFactory();

    @Override
    public boolean persist(@NotNull final String login, @NotNull final String password) throws Exception {
        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(PasswordParser.getPasswordHash(password));
        user.setRole(RoleType.ADMIN);
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.persist(user)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public boolean persist(@NotNull final String login, @NotNull final String password, @NotNull final RoleType role) throws Exception {
        @NotNull final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(PasswordParser.getPasswordHash(password));
        user.setRole(role);
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.persist(user)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public boolean persist(@NotNull User user) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.persist(user)) {
                sqlSession.commit();
                return true;
            }
            sqlSession.rollback();
        }
        return false;
    }

    @Override
    public Collection<User> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @Override
    public boolean checkPassword(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getLogin().equals(login)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPassword());
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkPasswordByUserId(@NotNull final String userId, @NotNull final String password) throws Exception {
        if (userId.isEmpty() || password.isEmpty()) throw new EmptyStringException();
        @Nullable final Collection<User> users = findAll();
        if (users != null) {
            for (User user : users) {
                if (user.getId().equals(userId)) {
                    return PasswordParser.getPasswordHash(password).equals(user.getPassword());
                }
            }
        }
        return false;
    }

    @Override
    public boolean updatePassword(@NotNull final String userId, @NotNull final String newPassword) throws Exception {
        if (userId.isEmpty() || newPassword.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.updatePassword(userId, PasswordParser.getPasswordHash(newPassword))) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public User findByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String login, @NotNull final String password,
                          @NotNull final RoleType role) throws Exception {
        if (userId.isEmpty() || login.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.update(userId, login, PasswordParser.getPasswordHash(password), role)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public boolean remove(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new EmptyStringException();
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAllByUserId(userId);
            if (userRepository.remove(userId)) {
                sqlSession.commit();
                return true;
            } else {
                sqlSession.rollback();
                return false;
            }
        }
    }

    @Override
    public void removeAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.removeAll();
            userRepository.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public boolean merge(@NotNull User user) throws Exception {
        try (@NotNull final SqlSession sqlSession = sessionFactory.openSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            if (userRepository.findByLogin(user.getLogin()) == null) {
                userRepository.persist(user);
                sqlSession.commit();
                return true;
            } else {
                userRepository.update(user.getId(), user.getLogin(), user.getPassword(), user.getRole());
                sqlSession.commit();
                return true;
            }
        }
    }

}
