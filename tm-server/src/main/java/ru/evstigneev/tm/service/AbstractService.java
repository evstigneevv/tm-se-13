package ru.evstigneev.tm.service;

import java.util.Collection;

public abstract class AbstractService<V> {

    public abstract void removeAll() throws Exception;

    public abstract Collection<V> findAll() throws Exception;

}
