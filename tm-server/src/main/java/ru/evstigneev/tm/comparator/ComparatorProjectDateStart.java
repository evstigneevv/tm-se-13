package ru.evstigneev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Project;

import java.util.Comparator;

public class ComparatorProjectDateStart implements Comparator<Project> {

    @Override
    public int compare(@NotNull final Project o1, @NotNull final Project o2) {
        if (o1.getDateStart() != null && o2.getDateStart() == null) {
            return -1;
        }
        if (o1.getDateStart() == null && o2.getDateStart() != null) {
            return 1;
        }
        if (o1.getDateStart() == null || o2.getDateStart() == null) {
            return 0;
        }
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
