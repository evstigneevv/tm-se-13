package ru.evstigneev.tm.util;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.api.repository.IProjectRepository;
import ru.evstigneev.tm.api.repository.ISessionRepository;
import ru.evstigneev.tm.api.repository.ITaskRepository;
import ru.evstigneev.tm.api.repository.IUserRepository;
import ru.evstigneev.tm.service.PropertyService;

import javax.sql.DataSource;

public class MyBatisUtil {

    @Nullable
    final PropertyService propertyService = new PropertyService();

    public SqlSessionFactory getSqlSessionFactory() {
        @Nullable final String login = propertyService.getDbLogin();
        @Nullable final String password = propertyService.getDbPassword();
        @Nullable final String url = propertyService.getUrl();
        @Nullable final String driver = propertyService.getDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, login, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
