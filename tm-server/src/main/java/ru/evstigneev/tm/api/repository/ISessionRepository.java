package ru.evstigneev.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;

public interface ISessionRepository {

    @Insert("insert into sessions values(#{id}, #{userId}, #{roleType}, #{timestamp}, #{signature})")
    boolean persist(@NotNull final Session session) throws Exception;

    @Delete("delete from sessions where user_id = #{userId}")
    boolean removeByUserId(@NotNull @Param("userId") final String userId) throws Exception;

}
