package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Session;
import ru.evstigneev.tm.enumerated.RoleType;

public interface ISessionService {

    void validate(@NotNull final Session session) throws Exception;

    void validate(@Nullable final Session session, @NotNull final RoleType roleType) throws Exception;

    Session openSession(@NotNull final String login, @NotNull final String password) throws Exception;

    boolean closeSession(@NotNull final Session session) throws Exception;

    boolean persist(@NotNull final String userId, @NotNull final Session session) throws Exception;

    boolean removeByUserId(@NotNull final String userId) throws Exception;

}
