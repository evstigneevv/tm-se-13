package ru.evstigneev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    String URL = "http://localhost:8080/SessionEndpoint?wsdl";

    @WebMethod
    void validate(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    Session openSession(@WebParam(name = "login") @NotNull final String login,
                        @WebParam(name = "password") @NotNull final String password) throws Exception;

    @WebMethod
    boolean closeSession(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean persist(@WebParam(name = "session") @NotNull final Session session) throws Exception;

    @WebMethod
    boolean removeByUserId(@WebParam(name = "session") @NotNull final Session session) throws Exception;

}
