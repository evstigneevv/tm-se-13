package ru.evstigneev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.Task;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Collection;
import java.util.Date;

public interface ITaskRepository {

    @Delete("delete from tasks where id = #{taskId} and user_id = #{userId}")
    boolean remove(@NotNull @Param("userId") final String userId, @NotNull @Param("taskId") final String taskId) throws Exception;

    @Update("update tasks set name = #{name}, description = #{description}, date_start = #{dateStart}, date_finish" +
            " = #{dateFinish}, status = #{status} where id = #{taskId} and user_id = #{userId}")
    boolean update(@NotNull @Param("userId") final String userId, @NotNull @Param("taskId") final String taskId,
                   @NotNull @Param("name") final String name, @NotNull @Param("description") final String description,
                   @NotNull @Param("dateStart") final Date dateStart,
                   @NotNull @Param("dateFinish") final Date dateFinish,
                   @NotNull @Param("status") final Status status) throws Exception;

    @Select("select * from tasks where project_id = #{projectId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "project_id", property = "projectId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Collection<Task> getTaskListByProjectId(@NotNull @Param("userId") final String userId,
                                            @NotNull @Param("projectId") final String projectId) throws Exception;

    @Select("select * from tasks")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "project_id", property = "projectId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Collection<Task> findAll() throws Exception;

    @Select("select * from tasks where user_id = #{userId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "project_id", property = "projectId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Collection<Task> findAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Delete("delete from tasks where user_id = #{userId} and project_id = #{projectId}")
    boolean deleteAllProjectTasks(@NotNull @Param("userId") final String userId,
                                  @NotNull @Param("projectId") final String projectId) throws Exception;

    @Select("select * from tasks where id = #{taskId} and user_id = #{userId} and project_id = #{projectId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "project_id", property = "projectId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Task findOne(@NotNull @Param("userId") final String userId, @NotNull @Param("projectId") final String projectId,
                 @NotNull @Param("taskId") final String taskId) throws Exception;

    @Delete("delete from tasks")
    void removeAll() throws Exception;

    @Delete("delete from tasks where user_id = #{userId}")
    boolean removeAllByUserId(@NotNull @Param("userId") final String userId) throws Exception;

    @Insert("insert into tasks values(#{id}, #{userId}, #{projectId}, #{name}, #{description}, #{dateOfCreation}, " +
            "#{dateStart}, #{dateFinish}, #{status})")
    boolean persist(@NotNull final Task task) throws Exception;

}
