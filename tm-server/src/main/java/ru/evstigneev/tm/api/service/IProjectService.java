package ru.evstigneev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    Collection<Project> findAll() throws Exception;

    boolean create(@NotNull final String userId, @NotNull final String projectName,
                   @NotNull final String description) throws Exception;

    boolean persist(@NotNull final Project project) throws Exception;

    boolean remove(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    boolean update(@NotNull final String userId, @NotNull final String projectId,
                   @NotNull final String name, @Nullable final String description,
                   @Nullable final String dateStart, @Nullable final String dateFinish, @NotNull final Status status) throws Exception;

    Project findOne(@NotNull final String userId, @NotNull final String projectId) throws Exception;

    Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception;

    void removeAll() throws Exception;

    boolean removeAllByUserId(@NotNull final String userId) throws Exception;

    List<Project> sort(@NotNull final String comparator) throws Exception;

    List<Project> searchByString(@NotNull final String string) throws Exception;

    boolean merge(Project project) throws Exception;

}
