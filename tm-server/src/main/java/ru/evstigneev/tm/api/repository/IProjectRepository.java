package ru.evstigneev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.evstigneev.tm.entity.Project;
import ru.evstigneev.tm.enumerated.Status;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface IProjectRepository {

    @Delete("delete from projects where id = #{projectId} and user_id = #{userId}")
    boolean remove(@NotNull @Param("userId") final String userId,
                   @NotNull @Param("projectId") final String projectId) throws SQLException;

    @Update("update projects set name = #{name}, description = #{description}, date_start = #{dateStart}, date_finish" +
            " = #{dateFinish}, status = #{status} where id = #{id} and user_id = #{userId}")
    boolean update(@NotNull @Param("userId") final String userId, @NotNull final @Param("id") String projectId,
                   @NotNull @Param("name") final String name, @Nullable final @Param("description") String description,
                   @Nullable @Param("dateStart") final Date dateStart,
                   @Nullable @Param("dateFinish") final Date dateFinish,
                   @NotNull @Param("status") final Status status) throws SQLException;

    @Select("select * from projects")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Collection<Project> findAll() throws Exception;

    @Select("select * from projects where user_id = #{userId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Collection<Project> findAllByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @Select("select * from projects where id = #{projectId} and user_id = #{userId}")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "user_id", property = "userId"),
            @Result(column = "name", property = "name"),
            @Result(column = "description", property = "description"),
            @Result(column = "date_of_creation", property = "dateOfCreation"),
            @Result(column = "date_start", property = "dateStart"),
            @Result(column = "date_finish", property = "dateFinish"),
            @Result(column = "status", property = "status"),
    })
    Project findOne(@NotNull @Param("userId") final String userId,
                    @NotNull @Param("projectId") final String projectId) throws SQLException;

    @Insert("insert into projects values(#{id}, #{userId}, #{name}, #{description}, #{dateOfCreation}, " +
            "#{dateStart}, #{dateFinish}, #{status})")
    boolean persist(@NotNull final Project project) throws SQLException;

    @Delete("delete from projects where user_id = #{userId}")
    boolean removeAllByUserId(@NotNull @Param("userId") final String userId) throws SQLException;

    @Delete("delete from projects")
    void removeAll() throws SQLException;

}
