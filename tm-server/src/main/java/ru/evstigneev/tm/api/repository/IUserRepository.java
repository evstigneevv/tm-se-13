package ru.evstigneev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.entity.User;
import ru.evstigneev.tm.enumerated.RoleType;
import ru.evstigneev.tm.exception.EmptyStringException;

import java.sql.SQLException;
import java.util.Collection;

public interface IUserRepository {

    @Update("update users set password = #{newPassword}  where id = #{userId}")
    boolean updatePassword(@NotNull @Param("userId") final String userId,
                           @NotNull @Param("newPassword") final String newPassword) throws SQLException;

    @Select("select id, login, password, role from users where login = #{login}")
    User findByLogin(@NotNull @Param("login") final String login) throws EmptyStringException, SQLException;

    @Select("select id, login, password, role from users")
    Collection<User> findAll() throws SQLException;

    @Update("update users set login = #{login}, password = #{password}, role = #{role} where id = #{userId}")
    boolean update(@NotNull @Param("userId") final String userId, @NotNull @Param("login") final String login,
                   @NotNull @Param("password") final String password,
                   @NotNull @Param("role") final RoleType role) throws SQLException;

    @Delete("delete from users where id = #{userId}")
    boolean remove(@NotNull @Param("userId") final String userId) throws SQLException;

    @Insert("insert into users values(#{id}, #{login}, #{password}, #{role})")
    boolean persist(@NotNull final User user) throws SQLException;

    @Delete("delete from users")
    void removeAll() throws SQLException;

}
