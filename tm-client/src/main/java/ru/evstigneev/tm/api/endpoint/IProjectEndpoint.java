package ru.evstigneev.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-02-06T17:46:56.718+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", name = "IProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/searchProjectByStringRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/searchProjectByStringResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/searchProjectByString/Fault/Exception")})
    @RequestWrapper(localName = "searchProjectByString", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SearchProjectByString")
    @ResponseWrapper(localName = "searchProjectByStringResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SearchProjectByStringResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.Project> searchProjectByString(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "string", targetNamespace = "")
                    java.lang.String string
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/persistProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/persistProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/persistProject/Fault/Exception")})
    @RequestWrapper(localName = "persistProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.PersistProject")
    @ResponseWrapper(localName = "persistProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.PersistProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean persistProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "project", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Project project
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeProject/Fault/Exception")})
    @RequestWrapper(localName = "removeProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveProject")
    @ResponseWrapper(localName = "removeProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjectsByUserIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjectsByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjectsByUserId/Fault/Exception")})
    @RequestWrapper(localName = "findAllProjectsByUserId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllProjectsByUserId")
    @ResponseWrapper(localName = "findAllProjectsByUserIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllProjectsByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.Project> findAllProjectsByUserId(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/createProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/createProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/createProject/Fault/Exception")})
    @RequestWrapper(localName = "createProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CreateProject")
    @ResponseWrapper(localName = "createProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CreateProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean createProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "projectName", targetNamespace = "")
                    java.lang.String projectName,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjectsByUserIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjectsByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjectsByUserId/Fault/Exception")})
    @RequestWrapper(localName = "removeAllProjectsByUserId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllProjectsByUserId")
    @ResponseWrapper(localName = "removeAllProjectsByUserIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllProjectsByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeAllProjectsByUserId(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findOneProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findOneProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findOneProject/Fault/Exception")})
    @RequestWrapper(localName = "findOneProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindOneProject")
    @ResponseWrapper(localName = "findOneProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindOneProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.evstigneev.tm.api.endpoint.Project findOneProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/updateProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/updateProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/updateProject/Fault/Exception")})
    @RequestWrapper(localName = "updateProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.UpdateProject")
    @ResponseWrapper(localName = "updateProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.UpdateProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean updateProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId,
            @WebParam(name = "newProjectName", targetNamespace = "")
                    java.lang.String newProjectName,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description,
            @WebParam(name = "dateStart", targetNamespace = "")
                    java.lang.String dateStart,
            @WebParam(name = "dateFinish", targetNamespace = "")
                    java.lang.String dateFinish,
            @WebParam(name = "status", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Status status
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjectsRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjectsResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/removeAllProjects/Fault/Exception")})
    @RequestWrapper(localName = "removeAllProjects", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllProjects")
    @ResponseWrapper(localName = "removeAllProjectsResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveAllProjectsResponse")
    public void removeAllProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjectsResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/findAllProjects/Fault/Exception")})
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.FindAllProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.Project> findAllProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/sortProjectsRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/sortProjectsResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/sortProjects/Fault/Exception")})
    @RequestWrapper(localName = "sortProjects", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SortProjects")
    @ResponseWrapper(localName = "sortProjectsResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.SortProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.evstigneev.tm.api.endpoint.Project> sortProjects(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "comparatorName", targetNamespace = "")
                    java.lang.String comparatorName
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/mergeProjectRequest", output = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/mergeProjectResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/IProjectEndpoint/mergeProject/Fault/Exception")})
    @RequestWrapper(localName = "mergeProject", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.MergeProject")
    @ResponseWrapper(localName = "mergeProjectResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.MergeProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean mergeProject(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session,
            @WebParam(name = "project", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Project project
    ) throws Exception_Exception;

}
