package ru.evstigneev.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-02-06T17:46:57.265+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/openSessionRequest", output = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/openSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/openSession/Fault/Exception")})
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.evstigneev.tm.api.endpoint.Session openSession(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/closeSessionRequest", output = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/closeSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/closeSession/Fault/Exception")})
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.CloseSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean closeSession(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/removeByUserIdRequest", output = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/removeByUserIdResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/removeByUserId/Fault/Exception")})
    @RequestWrapper(localName = "removeByUserId", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveByUserId")
    @ResponseWrapper(localName = "removeByUserIdResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.RemoveByUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean removeByUserId(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/persistRequest", output = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/persistResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/persist/Fault/Exception")})
    @RequestWrapper(localName = "persist", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.Persist")
    @ResponseWrapper(localName = "persistResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.PersistResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean persist(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/validateRequest", output = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/validateResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.evstigneev.ru/ISessionEndpoint/validate/Fault/Exception")})
    @RequestWrapper(localName = "validate", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.Validate")
    @ResponseWrapper(localName = "validateResponse", targetNamespace = "http://endpoint.api.tm.evstigneev.ru/", className = "ru.evstigneev.tm.api.endpoint.ValidateResponse")
    public void validate(
            @WebParam(name = "session", targetNamespace = "")
                    ru.evstigneev.tm.api.endpoint.Session session
    ) throws Exception_Exception;

}
