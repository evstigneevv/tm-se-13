package ru.evstigneev.tm.api.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for updateTask complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="updateTask"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://endpoint.api.tm.evstigneev.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="taskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taskName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateStart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dateFinish" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://endpoint.api.tm.evstigneev.ru/}status" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateTask", propOrder = {
        "session",
        "taskId",
        "taskName",
        "description",
        "dateStart",
        "dateFinish",
        "status"
})
public class UpdateTask {

    protected Session session;
    protected String taskId;
    protected String taskName;
    protected String description;
    protected String dateStart;
    protected String dateFinish;
    @XmlSchemaType(name = "string")
    protected Status status;

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the taskId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Sets the value of the taskId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

    /**
     * Gets the value of the taskName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTaskName() {
        return taskName;
    }

    /**
     * Sets the value of the taskName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTaskName(String value) {
        this.taskName = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the dateStart property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDateStart() {
        return dateStart;
    }

    /**
     * Sets the value of the dateStart property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDateStart(String value) {
        this.dateStart = value;
    }

    /**
     * Gets the value of the dateFinish property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDateFinish() {
        return dateFinish;
    }

    /**
     * Sets the value of the dateFinish property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDateFinish(String value) {
        this.dateFinish = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return possible object is
     * {@link Status }
     */
    public Status getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value allowed object is
     *              {@link Status }
     */
    public void setStatus(Status value) {
        this.status = value;
    }

}
