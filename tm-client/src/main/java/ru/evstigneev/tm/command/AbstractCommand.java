package ru.evstigneev.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.bootstrap.ServiceLocator;

public abstract class AbstractCommand {

    public ServiceLocator bootstrap;

    public void setBootstrap(@NotNull final ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

}