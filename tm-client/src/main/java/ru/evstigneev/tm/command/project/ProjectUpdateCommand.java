package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.Status;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public String command() {
        return "UP";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("UPDATE PROJECT");
        System.out.println("enter project ID: ");
        @NotNull final String projectId = bootstrap.getScanner().nextLine();
        System.out.println("enter project name: ");
        @NotNull final String projectName = bootstrap.getScanner().nextLine();
        System.out.println("enter description: ");
        @NotNull final String description = bootstrap.getScanner().nextLine();
        System.out.println("enter project start date: ");
        @NotNull final String dateStart = bootstrap.getScanner().nextLine();
        System.out.println("enter project finish date: ");
        @NotNull final String dateFinish = bootstrap.getScanner().nextLine();
        System.out.println("enter project status: ");
        @NotNull final String status = bootstrap.getScanner().nextLine();
        bootstrap.getProjectEndpoint().updateProject(bootstrap.getSession(), projectId, projectName, description,
                dateStart, dateFinish, Status.valueOf(status));
        System.out.println();
    }

}
