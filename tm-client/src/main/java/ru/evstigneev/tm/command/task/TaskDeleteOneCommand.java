package ru.evstigneev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.command.AbstractCommand;

public class TaskDeleteOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DOT";
    }

    @Override
    public String description() {
        return "Delete one task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter task ID: ");
        @NotNull final String taskId = bootstrap.getScanner().nextLine();
        if (bootstrap.getTaskEndpoint().removeTask(bootstrap.getSession(), taskId)) {
            System.out.println("Tasks was deleted!");
        } else {
            System.out.println("Tasks wasn't deleted!");
        }
    }

}
