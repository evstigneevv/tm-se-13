package ru.evstigneev.tm.command.data;

import ru.evstigneev.tm.command.AbstractCommand;

public class DataReadJaxbJsonCommand extends AbstractCommand {

    @Override
    public String command() {
        return "DATA READ JAXB JSON";
    }

    @Override
    public String description() {
        return "Read data from .json file";
    }

    @Override
    public void execute() throws Exception {
        bootstrap.getDomainEndpoint().readDataJaxbJson(bootstrap.getSession());
    }

}
