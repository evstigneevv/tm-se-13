package ru.evstigneev.tm.command.user;

import ru.evstigneev.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

    @Override
    public String command() {
        return "LO";
    }

    @Override
    public String description() {
        return "Log out current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOG OUT");
        bootstrap.getSessionEndpoint().closeSession(bootstrap.getSession());
        bootstrap.setSession(null);
        System.out.println("You log out successfully!");
    }

}
