package ru.evstigneev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.evstigneev.tm.api.endpoint.Project;
import ru.evstigneev.tm.command.AbstractCommand;

public class ProjectSearchByString extends AbstractCommand {

    @Override
    public String command() {
        return "SPS";
    }

    @Override
    public String description() {
        return "Search projects by string";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Enter a string for search: ");
        @NotNull final String searchingString = bootstrap.getScanner().nextLine();
        for (Project project : bootstrap.getProjectEndpoint().searchProjectByString(bootstrap.getSession(), searchingString)) {
            System.out.println("User ID: " + project.getUserId() + " | Project ID: " + project.getId() + " | Project name: "
                    + project.getName() + " | Project description: " + project.getDescription() + " | Date of creation: "
                    + project.getDateOfCreation() + " | Date of start: " + project.getDateStart() + " | Date of finish: "
                    + project.getDateFinish() + " | Project status: " + project.getStatus());
        }
        System.out.println();
    }

}
